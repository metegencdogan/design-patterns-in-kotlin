package creationalPatterns.factory

import java.lang.RuntimeException

open class Dog(override val name: String = "Dog", override val breed: String = "", override val id: Int) : Animal

class Bulldog(override val breed: String = "Bulldog", override val id: Int) : Dog(id = id)

class Beagle(override val breed: String = "Beagle", override val id: Int) : Dog(id = id)

open class Cat(override val name: String = "Cat", override val breed: String = "", override val id: Int) : Animal

class Persian(override val breed: String = "Persian", override val id: Int) : Cat(id = id)

class Siamese(override val breed: String = "Siamese", override val id: Int) : Dog(id = id)

interface Animal {
    val id: Int
    val name: String
    val breed: String
}

class DogFactory {
    fun createAnimal(animalBreed: String, id: Int): Dog {
        return when (animalBreed.trim().lowercase()) {
            "bulldog" -> Bulldog(id = id)
            "beagle" -> Beagle(id = id)
            else -> throw RuntimeException("Unknown dog breed $animalBreed")
        }
    }
}

class CatFactory {
    fun createAnimal(animalBreed: String, id: Int): Animal {
        return when (animalBreed.trim().lowercase()) {
            "persian" -> Persian(id = id)
            "siamese" -> Siamese(id = id)
            else -> throw RuntimeException("Unknown cat type $animalBreed")
        }
    }
}

class AnimalFactory {
    var counter = 0
    private val dogFactory = DogFactory()
    private val catFactory = CatFactory()
    fun createAnimal(animalType: String, animalBreed: String): Animal {
        return when (animalType.trim().lowercase()) {
            "cat" -> catFactory.createAnimal(animalBreed, ++counter)
            "dog" -> dogFactory.createAnimal(animalBreed, ++counter)
            else -> throw RuntimeException("Unknown animal $animalType breed $animalBreed")
        }
    }
}

fun main() {
    val factory = AnimalFactory()
    val animalTypes = listOf("cat" to "persian", "dog" to "bulldog", "dog" to "beagle", "cat" to "siamese")
    for ((type, breed) in animalTypes) {
        val c = factory.createAnimal(type, breed)
        println("${c.id} - ${c.name} - ${c.breed}")
    }
}