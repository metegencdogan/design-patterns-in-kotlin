package creationalPatterns.factory

class StaticFactory private constructor() {

    companion object {
        fun create(): StaticFactory {
            return StaticFactory()
        }
    }
}

fun main() {
    val staticFactory = StaticFactory.create()
}