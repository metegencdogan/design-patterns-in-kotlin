package creationalPatterns.singleton

object SingletonObject {
    init {
        //Singleton of Kotlin must not have a regular constructor, so we can use init block instead of
    }

    fun sayHello() = println("Hello, Singleton!!!")
}

fun main() {
    SingletonObject.sayHello()
}