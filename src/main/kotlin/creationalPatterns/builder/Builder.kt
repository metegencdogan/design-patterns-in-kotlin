package creationalPatterns.builder

data class Builder private constructor(
    private var _id: Int? = null,
    private var _firstname: String = "",
    private var _lastname: String = "",
    private var _phoneNumber: String? = null,
) {

    companion object {
        fun builder(): Builder {
            return Builder()
        }
    }

    fun id(id: Int?): Builder {
        _id = id
        return this
    }

    fun firstname(firstname: String): Builder {
        _firstname = firstname
        return this
    }

    fun lastname(lastname: String): Builder {
        _lastname = lastname
        return this
    }

    fun phoneNumber(phoneNumber: String?): Builder {
        _phoneNumber = phoneNumber
        return this
    }

    fun build(): Builder {
        val obj = Builder()
        obj._id = this._id
        obj._firstname = this._firstname
        obj._lastname = this._lastname
        obj._phoneNumber = this._phoneNumber
        return obj
    }
}

fun main() {
    val builderObj = Builder.builder()
        .id(1)
        .firstname("builder")
        .lastname("pattern")
        .phoneNumber("123456")
        .build()

    println(builderObj)
}